#!/usr/bin/python3
import serial
import time
import subprocess
from multiprocessing import Pool as ThreadPool

'''
# Ganged programming with AVRDUDE and up to 8 USBasp's

Author: Anders Draagen (andersdra@gmail.com)

# example usage: n : hardcoded in USBasp firmware
# fuses : low, high, extended

prog_list= [ UsbaspGang(number=0, part='m328p', flash='number0.hex', eeprom='number0.eep', fuses=[ 0xff, 0xff, 0xff ]),
             UsbaspGang(number=1, part='t85', flash='number1.hex', fuses=[ 0xff, 0xff, 0xff ]),
             UsbaspGang(number=2, part='t45', flash='number2.hex', a='-B1 -e', fuses=[ 0xff, 0xff, 0xff ], retries=1)]
gangbang = GangMultiProcess(p=prog_list) # multiprocess programming
error = gangbang.thread() # program
if error:
    print(error) # prog[x] 0 if ok, 1 if failed
else:
    # all programmed OK
'''

avrdude_config = "./avrdude/avrdude.conf"
avrdude_fuse_strings = (" -U lfuse:w:", " -U hfuse:w:", " -U efuse:w:")
avrdude_error_list = "initialization failed, could not find USB device, verification error, checksum mismatch, reading back fuses was unreliable"

class GangMultiProcess(object):

    def __init__(self, p):
        self.num_programmers = len(p)
        self.programmers = p


    def thread(self):
        swim = []
        with ThreadPool(self.num_programmers) as pool:
            swim = pool.map(self.program, self.programmers)
        return swim


    def program(self, programmer):
        output = programmer.program()
        return output


def avrdude_build_string(flash, eeprom, fuses):
        avrdude_string = ''
        expected_passes = 0
        if fuses:
            zero_count = 0
            for count, fuse in enumerate(fuses):
                if fuse != 0:
                    avrdude_string += (avrdude_fuse_strings[count] + str(hex(fuse))) + ":m"
                else:
                    zero_count += 1
            if zero_count != 3:
                expected_passes += 1
            else:
                fuses = [] # if list with zero byte fuses is input, this fixes it
        if flash:
            avrdude_string += " -U flash:w:" + flash + ":i"
            expected_passes += 1
        if eeprom:
            avrdude_string += " -U eeprom:w:" + eeprom + ":i"
            expected_passes += 1
        return [avrdude_string, expected_passes]


class UsbaspGang(object):
    # input fuses as low,high,extended
    def __init__(self, number, part, flash="", eeprom='', fuses=[], args='', retries=3):
        self.flash_done = 0
        self.eeprom_done = 0
        self.fuses_done = 0
        self.retries = 0
        self.max_retries = retries
        self.name = "prodasp" + str(number)
        self.number = number
        self.part = part
        self.flash = flash
        self.eeprom = eeprom
        self.args = args
        self.fuses = fuses
        self.expected_passes = 0
        self.avrdude = "avrdude" + " -p " + self.part + " -c " + self.name
        config = avrdude_build_string(self.flash, self.eeprom, self.fuses)
        self.expected_passes = config[1]
        self.avrdude += config[0] + " " + self.args + " -C " + avrdude_config
        print("SELF: ", self.avrdude, self.expected_passes)
        self.passes = 0
        self.programmed = 0
        self.lastprogramming = 0

    # program AVR, catch errors
    def program(self):
        print(self.avrdude)
        try:
            avrdude_output = subprocess.check_output(self.avrdude.split(' '), stderr=subprocess.STDOUT, shell=False)
            print(avrdude_output)
            if utf8bytes("Fuses OK") in avrdude_output:
                print("programmer: {} fuses ok!").format(self.number)
                self.fuses_done = True
                self.passed()
            if utf8bytes("flash verified") in avrdude_output:
                print("programmer: {} flash verified!").format(self.number)
                self.flash_done = True
                self.passed()
            if utf8bytes("eeprom verified") in avrdude_output:
                print("programmer: {} eeprom verified!").format(self.number)
                self.eeprom_done = True
                self.passed()
        except subprocess.CalledProcessError as avrdude_exec:
            print("programmer {} error!\n{}\nRETRYING!".format(self.number, avrdude_exec.output))
            self.failed()
        if self.expected_passes == self.passes:
           return False
        else:
           return True


    def passed(self):
        self.passes += 1


    def failed(self):
        self.retries += 1
        if self.retries < self.max_retries:
            print("Sleeping 1 sec: N: {}".format(self.number))
            time.sleep(1)
            self.program()
        else:
            print("RETRIES EXCEEDED! programmer {}".format(self.number))



def utf8bytes(inputstring):
    return bytes(inputstring.encode('utf-8'))

# list of programmers
programmer_list = [ UsbaspGang(number=0, part="t85", flash="testhex.hex", args="-B 1"),
                    UsbaspGang(number=1, part="m328p", flash="ttt.hex"),
                    UsbaspGang(number=2, part="m328p", fuses=[0x69, 0x69, 0x96], retries=1),
                    UsbaspGang(number=3, part="m328p")]

start_time  = time.perf_counter()

# setup Pool, gang-program and check for error
gangbang = GangMultiProcess(p=programmer_list)
error = gangbang.thread()
if error:
    for programmer, not_finished in enumerate(error):
        if not_finished:
            print("Programmer {} did not finish".format(programmer))
else:
    print("Program NEXT::")

stop_time = time.perf_counter()
time_elapsed = stop_time - start_time
print("Time elapsed: {}".format(time_elapsed))
