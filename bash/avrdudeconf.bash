#!/bin/bash
# place together with avrdude.conf
# makes 8 separate USBasp entries prodasp0-7 with different product ids and appends to config file

cp avrdude.conf avrdude.conf_bak
for i in {0..7}
do
    echo "
programmer parent \"usbasp\"
id    = \"prodasp$i\";
usbproduct = \"ProductionUSBasp$i\";
;" >> avrdudeconf_temp
done
cat avrdudeconf_temp >> avrdude.conf
rm avrdudeconf_temp
