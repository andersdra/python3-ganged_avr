#!/usr/bin/bash
# makes USBasp devices easily distinguishable

num_programmers=8
avrdude_config="$1"

count=0
while [ $count -le $((num_programmers-1)) ]
do
    echo "Connect programmer $count then press return"
    read
    make clean
    sed -i "/USB_CFG_DEVICE_NAME/c\#define USB_CFG_DEVICE_NAME   \
    'p', 'r', 'o', 'd', 'a', 's', 'p', '$count'" usbconfig.h
    make main.hex
    avrdude -C "$avrdude_config" -p m8 -c usbasp -U flash:w:main.hex:i
    count=$((count+1))
done
