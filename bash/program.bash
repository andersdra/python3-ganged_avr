#!/bin/bash

config='/home/tc/avrdude.conf'

for i in {0..7}
do
    echo "Connect programmer $i then press enter"
    read
    avrdude -C "$config" -p m8 -c usbasp -U flash:w:main"$i".hex:i
done
