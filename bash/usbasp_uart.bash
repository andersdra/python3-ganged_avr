#!/bin/bash

for i in {0..7}
do
    sed -i "/if(strcmp(\"prodasp/c\			if(strcmp(\"prodasp$i\", (const char*)str)){" usbasp_uart.c
    make
    mv usbasp_uart terms/usbasp_uart"$i"
done
