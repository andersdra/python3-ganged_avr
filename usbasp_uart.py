#!/usr/bin/python3
import subprocess
uart_binary='/location/to/usbasp_uart'

class USBaspUART(object):
    def __init__(self, n):
        self.number = n

    def sendbytes(self, inbytes):
        sendbytes = ['/bin/echo']
        for byte in inbytes:
            sendbytes.append(str(byte))
        p1 = subprocess.Popen(sendbytes, stdout=subprocess.PIPE)
        p2 = subprocess.Popen([uart_binary + str(self.number), '-w'],stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=p1.stdout)
        err = p2.communicate()
        print("ERR: ", err)


    def readbytes(self):
        print("read")


UART0 = USBaspUART(n=0)

UART0.sendbytes([0x69, 0x1])
